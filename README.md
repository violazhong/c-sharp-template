A simple repository with a few minor files.  Intended to be used as a location to experiment with C#.

For C#, I recommend using Visual Studio as the IDE.  Visual Studio comes with a free Community Edition: https://visualstudio.microsoft.com/vs/community/
