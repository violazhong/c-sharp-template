using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace c_sharp_playground.tests
{
    [TestClass]
    public class CalculatorTests
    {
        private Calculator _target;

        [TestInitialize]
        public void Initialize()
        {
            _target = new Calculator();
        }

        [TestMethod]
        public void Add_GivenZeros_ReturnsZero()
        {
            var result = _target.Add(0, 0);

            Assert.AreEqual(0, result);
        }
    }
}
